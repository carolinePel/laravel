<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function book(){
        //pourquoi pas hasMany?
        return $this->belongsToMany(Book::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title','description','genre_id'
    ];
    public function setGenreIdAttribute($value){
        $this->attributes['genre_id'] = ($value == 0)? null : $value;
    }
    public function genre(){
        return $this->belongsTo(Genre::class);
    }
    public function authors(){
        //pourquoi pas hasMany?
        return $this->belongsToMany(Author::class);
    }
    //arv
    public function picture(){
        return $this->hasOne(Picture::class);
    }
}

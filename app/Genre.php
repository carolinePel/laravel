<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public function books(){
        //le class renvoie le nameSpace qui est Book(qui se trouve dans le même espace)
        return $this->hasMany(Book::class);
    }
}

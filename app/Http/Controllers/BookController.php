<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;//importer l'alias de la classe
use App\Author;
use App\Genre;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::paginate(5);
        //return "Dashboard";
        return view('back.book.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::pluck('name','id')->all();
        $genres  = Genre::pluck('name','id')->all();
        return view('back.book.create',['authors'=>$authors,'genres'=>$genres]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //debuggage
        //dump($request->all()); die;

        /* validate 
        ** 'string|required' => pipé
        */
        $this->validate($request,[
            'title'       => 'string|required',
            'description' => 'string',
            'genre_id'    => 'integer',
            'authors.*'   => 'integer',
            'status'      => [ 'regex:/^mama|unpublished$/']
        ]);
        $book = Book::create($request->all());
        //dump($request->old('description'));die;
        // traitement des data
        //renvoie un objet qui permet de m'informer sur 
        //la ligne nouvellement crée dans la database
        //renvoie aussi les relations avec les tables 
        //liées à la ligne
        $authors = $request->authors; // array
        //dump($authors);
        //récupère l'id du nouveau book,
        //et crée un ligne dans la table authors_book
        //avec les clés secondaires
        $book->authors()->attach($authors);
       

        return redirect()->route('book.index')->with('message',[
            'type' => 'success',
            'content' => 'merci pour l\'ajout de votre livre'
        ]);
        //$book->title ="Un nouveau titre";

        //$book->save();
        // redirection
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * A la place d'injecter un id, on peut directement
     * injecter le Book $book, et  le show va regarder l'URI 
     * (ex:'admin/book/17'),
     * prendre le numéro de book, et l'utiliser pour retourner
     * la view => 
     * A ne faire que en back, car en front on pourra modifier
     * les URI
     */
    public function show(Book $book)
    {
        //Solution commentée utilisable si $id dans l'argument
        //$book = Book::find($id);
        return view('back.book.show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /* validate 
        ** 'string|required' => pipé
        */
        //dump($id);die;
        $book = Book::find($id);
        //dump($book->authors->pluck('id')->toArray());die;
        // $book->authors->map(function($author){
        //     echo $author->id." ";
        // });
        // die;
        //dump($book->id);die;
        $authors = Author::pluck('name','id')->all();
        $genres  = Genre::pluck('name','id')->all();
        return view('back.book.edit',['authors'=>$authors,'genres'=>$genres,'book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dump($errors);die;
        $this->validate($request,[
            'title'       => 'string|required',
            'description' => 'string',
            'genre_id'    => 'integer',
            'authors.*'   => 'integer',
            'status'      => [ 'regex:/^published|unpublished$/']
        ]);
        $book = Book::find($id);
        $book->update($request->all());
        $book->authors()->sync($request->authors);
        return redirect()->route('book.index')->with('message',[
            'type' => 'success',
            'content' => 'merci pour la modification de votre livre'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$book = Book::find($id);
        //$book->forceDelete();
    }
}

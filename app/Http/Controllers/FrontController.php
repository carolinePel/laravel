<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;//importer l'alias de la classe
use App\Author;
use App\Genre;

class FrontController extends Controller
{
    public function __construct(){
        view()->composer('partials.menu',function($view){
            //récupérer un tableau associatif
            $genres = Genre::pluck('name','id')->all();
            //passer les données à la vue
            $view->with('genres',$genres);
        });
    }
    public function index(){
        //return Book::all();
        $books = Book::paginate(5);

        // compact => avec une clé variable récupérée dans le contexte
        // (méthode ou fonction par exemple) compact('books') <=> ['books' => $books]
        return view('front.index', compact('books'));
    }
    public function show($id){
        //return Book::find($id);
        $book = Book::find($id);

        return view('front.show',compact('book'));
    }
    public function showBookByAuthor(int $id){
        $authorName = Author::find($id)->name;
        $books = Author::find($id)->book()->paginate(5);
        return view('front.author',compact('books','authorName'));
    }
    public function showBooksByGenre(int $id){
        $genre = Genre::find($id)->name;
        $books = Genre::find($id)->books()->paginate(5);
        return view('front.genre',compact('books','genre'));
    }
}

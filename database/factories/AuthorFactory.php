<?php

use Faker\Generator as Faker;

$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'name'   => $faker->unique()->name,
        'email'  => $faker->unique()->safeEmail,
        'phone'  => $faker->sentence(),
        'adress' => $faker->paragraph()
    ];
});

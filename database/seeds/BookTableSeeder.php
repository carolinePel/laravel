<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Genre::create([
            'name' =>'science'
        ]);
        App\Genre::create([
            'name' =>'Art'
        ]);
        App\Genre::create([
            'name' =>'Cookbook'
        ]);
        
        //A chaque fois que l'on relance l'insertion des données
        //on supprime les images se trouvant dans le dossier images
        Storage::disk('local')->delete(Storage::allFiles());
        
        //création de 30 livres à partir de la factory
        factory(App\Book::class,20)->create()->each(function($book){
            //prendre un genre
            $genre = App\Genre::find(rand(1,3));
            //création de la clé secondaire de book après avoir créé
            //les clés primaires dans genre
            //on dit que le livre belongsTo() au genre
            $book->genre()->associate($genre);
            //et on sauvegarde pour une relation one to one
            $book->save();
            /**AJOUT IMAGES */
            //créer un hash de lien pour le nom de l'image
            $link = str_random(12) . '.jpeg';
            $file = file_get_contents('http://via.placeholder.com/250/250/'.rand(1,9));
            //déplacer le flux récupéré
            Storage::disk('local')->put($link,$file);
            //création d'un enregistrement ds la table pictures
            //création automatique d'un identifiant 'book_picture'
            $book->picture()->create([
                'title'=>'Default',
                'link'=>$link
            ]);
            //méthode pluck-shuffle et slice permet de prendre au hasard dans la colonne id un numéro
            //all() effectue la requête et récupère le résultat sous forme de tableau
            $authors = App\Author::pluck('id')->shuffle()->slice(0,rand(1,3))->all();
            //$book->save(); // TODO voir si on peut s'en passer
            //mettre en relation les auteurs avec les books:
            //relation many to many ->belongsToMany()
            //pas besoin de save, le attach fait un save automatique
            $book->authors()->attach($authors);
            //pas besoin de sauvegarde pour une relation one to many
        });
    }
}

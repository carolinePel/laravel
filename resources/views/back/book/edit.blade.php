@extends('layouts.master')

@section('title')
Editer un livre
@endsection
@section('content')
<h1>Editer un livre</h1>
<form action="{{route('book.update',$book->id)}}" method='post'>
    {{csrf_field()}}
    {{method_field('PUT')}}
    <div class="form-row">
        <div class="col-7">
            <label for="title">Titre :</label>
            <input type="text" name="title" value="{{$book->title}}" class="form-control" id="title" placeholder="{{$book->title}}">       
            @if($errors->has('title'))
            <span class="error bg-warning text-warning">{{$errors->first('title')}}</span>
            @endif
            @if($errors->first('title'))
                <div class="alert alert-danger">
                    <p>{{$errors->first('title')}}</p>
                </div>
            @endif

            <label for="bookDesc">Description :</label>
            <textarea name="description" class="form-control" id="bookDesc" rows="3" value="">{{$book->description}}</textarea>
            @if($errors->first('description'))
                <div class="alert alert-danger">
                    <p>{{$errors->first('description')}}</p>
                </div>
            @endif

            <label for="genre">Genre :</label>
            <select name="genre_id" id='genre' class="">
                <option value="0">No genre</option>
                @forelse($genres as $key=>$genre)
                    @if(isset($book->genre->id) && $book->genre->id == $key)
                    <option value="{{$key}}" selected>{{$genre}}</option>
                    @else
                    <option value="{{$key}}">{{$genre}}</option>
                    @endif
                    @empty
                        <option>No Genre available</option>
                @endforelse
            </select>
            @if($errors->first('genre_id'))
                <div class="alert alert-danger">
                    <p>{{$errors->first('genre_id')}}</p>
                </div>
            @endif

            <h2>Choisissez un/des auteur(s)</h2>
            @forelse($authors as $id=>$name)
            <div class="form-check">
                @if(in_array($id,$book->authors->pluck('id')->toArray()))
                <input name="authors[]" class="form-check-input" type="checkbox" value={{$id}} id="author{{$id}}" checked/>
                @else
                <input name="authors[]" class="form-check-input" type="checkbox" value={{$id}} id="author{{$id}}"/>
                @endif
                <label class="form-check-label" for="author{{$id}}">
                    {{$name}}
                </label>
            </div>
            @empty
            <p>Pas d'auteur connu</p>
            @endforelse
            @if($errors->first('authors'))
                <div class="alert alert-danger">
                    <p>{{$errors->first('authors')}}</p>
                </div>
            @endif

        </div>
        <div class="col">
            <input type="submit" value="Modifier un livre"/>      
            
            <legend class="col-form-label">Radios</legend>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="status" id="gridRadios1" value="published" checked>
                <label class="form-check-label" for="gridRadios1">
                    Publier
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="unpublished">
                <label class="form-check-label" for="gridRadios2">
                    Dépublier
                </label>
            </div>
            @if($errors->first('status'))
                <div class="alert alert-danger">
                    <p>{{$errors->first('status')}}</p>
                </div>
            @endif

            <legend class="col-form-label">Choose an image</legend>
            <div class="custom-file">
                <input name="picture" type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
    </div>
</form>
@endsection
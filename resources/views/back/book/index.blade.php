@extends('layouts.master')

@section('title')
Liste des livres
@endsection
@section('content')
<h1>Liste des livres</h1>
{{$books->links()}}
@include('back.partials.flash')
@include('back.partials.errors')

<a href="{{route('book.create')}}"><button type="button" class="btn btn-primary btn-lg">Ajouter un livre</button></a>
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col"> Title</th>
            <th scope="col"> Author</th>
            <th scope="col"> Genre</th>
            <th scope="col"> Date de publication</th>
            <th scope="col"> Status</th>
            <th scope="col"> Edition</th>
            <th scope="col"> Show</th>
            <th scope="col"> Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($books as $book)
        <tr>
            <td>
                <a href="{{route('book.show',$book->id)}}">{{$book->title}}</a>
            </td>
            <td>
                @forelse($book->authors as $author)
                    {{$author->name}} {{$author->id}}
                    @empty
                    livre auto-généré
                @endforelse
            </td>
            <td>{{$book->genre->name??'aucun genre'}}</td>
            <td>{{$book->created_at}}</td>
            <td>Status:TODO</td>
            <td >
                <a href="{{route('book.edit',$book->id)}}"> Edit </a>
            </td>
            <td><a href="{{route('book.show', $book->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td>
                <form class='delete' action="{{route('book.destroy',$book->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('delete')}}
                <input class='delete' type='submit' value="delete"> 
                </form>
            </td>
        </tr>
        @endforeach
   </tbody>
</table>
{{$books->links()}}
@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('js/confirm.js')}}"></script>
@endsection
@extends('layouts.master')

@section('title')
Livre
@endsection

@section('content')


    <div class="row">
        <div class="col col-lg-6">
            <h1>Title: {{ $book->title }} </h1>

            <ul>
                <li>{{ __('book.category') }} {{$book->genre->name??'aucun genre'}}</li>
                <li>Date de création : {{$book->created_at}}</li>
                <li>Date de mise à jour : {{$book->updated_at}}</li>
                <li>Status:todo</li>
            </ul>

            <h3>Author(s)</h3>
            <ul>
            @forelse($book->authors as $author)
                <li><a href="{{url('author',$author->id)}}">{{$author->name}}</a></li>
                @empty
                <p>livre auto-généré</p>
            @endforelse
            </ul>
        </div>
        @if($book->picture)
            <div class="col"><img src="{{asset('images/'.$book->picture->link)}}" alt="{{$book->picture->title}}"></div>
        @endif
    </div>

@endsection
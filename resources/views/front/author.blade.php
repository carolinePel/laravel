@extends('layouts.master')

@section('title')
auteur
@endsection

@section('content')

<h1>{{$authorName}}</h1>
    {{$books->links()}}
<ul class="list-group">
@forelse($books as $book)
    <li class="list-group-item">
        @if($book->title)
        <h2><a href="{{url('book',$book->id)}}">{{$book->title}}</a></h2>
        @endif
        @if($book->description)
            <p>{{$book->description}}</p>
        @endif
        @if($book->picture)
        <img src="{{url('images/',$book->picture->link)}}" alt="{{$book->picture->title}}">
        @endif 
    </li>
@empty 
    <li>Aucun livre</li>
@endforelse
</ul>


@endsection
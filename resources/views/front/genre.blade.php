@extends('layouts.master')

@section('title')
Genre
@endsection

@section('content')
{{dump($books)}}
<h1>{{$genre}}</h1>
<ul class="list-group">

@forelse($books as $book)
    <li class="list-group-item">
        @if($book->title)
        <h2><a href="{{url('book',$book->id)}}">{{$book->title}}</a></h2>
        @endif
        @if($book->description)
        <p>{{$book->description}}</p>
        @endif
        @if($book->picture)
        <img src="{{url('images/',$book->picture->link)}}" alt="{{$book->picture->title}}">
        @endif 
        <h3>Author(s)</h3>
        <ul>
        @forelse($book->authors as $author)
            <li><a href="{{url('author',$author->id)}}">{{$author->name}}</a></li>
            @empty
            <p>livre auto-généré</p>
        @endforelse
        </ul>
    </li>
@empty 
    <li>Aucun livre</li>
@endforelse
</ul>
@endsection
@extends('layouts.master')

@section('title')
Page d'accueil
@endsection

@section('content')
    <h1>Les livres</h1>
    {{$books->links()}}
<ul class="list-group">
@forelse($books as $book)
    <li class="list-group-item">
        @if($book->title)
        <h2><a href="{{url('book',$book->id)}}">{{$book->title}}</a></h2>
        @endif
        @if($book->description)
            <p>{{$book->description}}</p>
        @endif
        <ul>
        @forelse($book->authors as $author)
            <li><a href="{{url('author',$author->id)}}">{{$author->name}} {{$author->id}}</a></li>
            @empty
            <p>livre auto-généré</p>
        @endforelse
        </ul>
        @if($book->picture)
        <img src="{{url('images/',$book->picture->link)}}" alt="{{$book->picture->title}}">
        @endif 
    </li>
@empty 
    <li>Aucun livre</li>
@endforelse
</ul>


@endsection

@section('sidebar')
@parent 
<!-- <ul>
    <li>1</li>
    <li>2</li>
</ul> -->
@endsection
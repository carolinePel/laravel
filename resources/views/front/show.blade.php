@extends('layouts.master')

@section('title')
Livre
@endsection

@section('content')
{{dump($book)}}
<h1>{{ $book->title }} </h1>
@if($book->picture)
    <img src="{{url('images/',$book->picture->link)}}" alt="{{$book->picture->title}}">
<h2>Description</h2>
@endif
<p>{{ $book->description }}</p>
<h3>Author(s)</h3>
<ul>
@forelse($book->authors as $author)
    <li><a href="{{url('author',$author->id)}}">{{$author->name}}</a></li>
    @empty
    <p>livre auto-généré</p>
@endforelse
</ul>
@endsection
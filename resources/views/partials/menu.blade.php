<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
<a class="navbar-brand" href="#">LARAVEL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">        
        <ul class="navbar-nav">
            <li class="nav-item active"><a class="nav-link" href="{{url('')}}">Acceuil <span class="sr-only">(current)</span></a></li>
            @if(!Route::is('book.*'))
                @forelse($genres as $id=>$genre)
                <li class="nav-item"><a class="nav-link" href="{{url('genre',$id)}}">{{$genre}}</a></li>
                @empty
                <li>vide</li>
                @endforelse
            @endif
            @if(Auth::check())
            <li><a href="{{route('book.index')}}">Dashboard</a></li>
            <li><a 
                    href="{{route('logout')}}"
                    onclick="event.preventDfault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{csrf_field()}}
                </form>
            </li>
            @else
            <li><a href="{{route('login')}}">Login</a></li>
            @endif
        </ul>

     
    </div>
</nav>

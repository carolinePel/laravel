<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
    //return ('pouet'); 
});
//remplaçait la route à la racine et
//dans le fichier FrontController on mettati ce que l'on voulait
//qui soit affiché
//Route::get('/','FrontController@index');

//afficher tous les livres:
Route::get('/boooks',function(){
    //$params = $slug;
    return App\Book::all();
});

Route::get('books',function(string $word1,string $word2){
    $search = $word1.' '.$word2;
    $result = App\Book::where('title','like',"%$search")->get();
    if(count($result)==0)
        return "sorry not found";
    return $result;
});
//si on veut passer un paramètre dynamique, on peut passer une variable
//dans la fonction
// :: => instance et sa méthode dans laravel
//route est instancié même si ça se voit pas
//pourquoi on peut pas faire
//Route::get('books/{id})'
// Route::get('book/{id}',function($id){
//     return App\Book::find($id);
// });

Route::get('/','FrontController@index');
//where définit la Regex qui permet de paramétrer le type d'uri que l'on aura
Route::get('book/{id}','FrontController@show')->where(['id' =>'[1-9][0-9]*']);

Route::get('author/{id}','FrontController@showBookByAuthor')->where(['id' =>'[1-9][0-9]*']);
Route::get('genre/{id}','FrontController@showBooksByGenre')->where(['id' =>'[1-9][0-9]*']);
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//routes pour login laravel avec commande
//php artisan make:auth
//Auth::routes();
//routes sécurisées
Route::resource('admin/book','BookController')->middleware('auth');